import math
import re
import json
import time
import logging
import sys
from queue import Queue
from threading import Thread, Lock
from bs4 import BeautifulSoup
from profiles import ENVIRONMENT
from helpers.scraper import Scraper
import requests

OUTPUT_PATH = "/Users/Ahmed/Desktop/evals_output.json"

PAGINATION_REGEX = "Total (.*)"
LECTURE_SECTION_REGEX = ".*-(.*)"

PAGE_SIZE = 100

DEPT_INDEX = 0
LNAME_INDEX = 2
FNAME_INDEX = 3
TERM_INDEX = 4
YEAR_INDEX = 5
ENTHUSIASM_INDEX = 12
WORKLOAD_INDEX = 13
RECOMMEND_INDEX = 14
INVITED_INDEX = 15
COMPLETED_INDEX = 16

# Basic logging configuration, will refactor and set in parent
logging.basicConfig(
    level=logging.INFO,
    format="[%(asctime)s][%(levelname)s][%(name)s.%(funcName)s:%(lineno)d] %(message)s",
    datefmt="%H:%M:%S",
    stream=sys.stdout)


class CourseEvaluationsScraper:
    """
    A scraper for the course evaluations at UofT.

    This class scrapes the host by performing a blanket search for all course evaluations,
    paginating through all the responses, and scrapes each evaluation.
    """
    HOST = "https://course-evals.utoronto.ca"
    EVALS_SEARCH_PATH = ""

    EVALS_REQUEST_PATH = "/BPI/fbview-WebService.asmx/getFbvGrid"

    EVALS_SEARCH_PAYLOAD = {
                    "strUiCultureIn": "en-US",
                    "datasourceId": "7160",
                    "blockId": "900",
                    "subjectColId": "1",
                    "subjectValue": "____[-1]____",
                    "detailValue": "____[-1]____",
                    "gridId": "fbvGrid",
                    "pageActuelle": 1,
                    "strOrderBy": ["col_1", "asc"],
                    "strFilter": ["", "", "ddlFbvColumnSelectorLvl1", ""],
                    "sortCallbackFunc": "__getFbvGrid",
                    "userid": "lScCagHyrxZS3UF2niK3xH5LJ_HssVuhRoCM",
                    "pageSize": "1"
                }

    logger = logging.getLogger('course-evals-scraper')

    lock = Lock()
    RESULTS = []
    ERROR_COUNT = 0

    @staticmethod
    def scrape():
        """
        Generate the jobs to scrape the host website, queue them,
        and assign them to workers.
       :return: A list of JSON course evaluation information
        """
        start_time = time.time()
        q = Queue()
        job_count, eval_count = CourseEvaluationsScraper.get_job_count()
        CourseEvaluationsScraper.logger.info("%s jobs found, setting up workers.", job_count)

        for i in range(job_count):
            job_payload = CourseEvaluationsScraper.EVALS_SEARCH_PAYLOAD.copy()
            job_payload["pageActuelle"] = i+1
            job_payload["pageSize"] = PAGE_SIZE
            q.put(job_payload)

        for i in range(ENVIRONMENT.scraper.maxThreads):
            CourseEvaluationsScraper.logger.info("Provisioning a new thread")
            t = Thread(target=CourseEvaluationsScraper.scraper_worker, args=(q,i))
            t.start()
        q.join()

        total = len(CourseEvaluationsScraper.RESULTS)
        elapsed_time = time.time() - start_time
        time_taken = time.strftime("%H:%M:%S", time.gmtime(elapsed_time))
        CourseEvaluationsScraper.logger.info("Successfully scraped %s out of %s, time taken is %s", total, eval_count,
                                             time_taken)

        all_course_evaluations = CourseEvaluationsScraper.RESULTS.copy()
        CourseEvaluationsScraper.RESULTS, CourseEvaluationsScraper.ERROR_COUNT = [], 0
        return all_course_evaluations

    @staticmethod
    def get_job_count():
        """

        :return: The integer number of jobs needed to scrape the host
        """
        all_course_evals_base_url = CourseEvaluationsScraper.HOST + CourseEvaluationsScraper.EVALS_REQUEST_PATH
        professor_search_results = Scraper.get_web_page(all_course_evals_base_url, method='POST',
                                                        payload=CourseEvaluationsScraper.EVALS_SEARCH_PAYLOAD)
        pagination_term_search = re.search(PAGINATION_REGEX, professor_search_results["d"][2],
                                           re.IGNORECASE)
        eval_count = int(pagination_term_search[1])
        job_count = math.ceil(eval_count / PAGE_SIZE)
        return job_count, eval_count

    @staticmethod
    def scraper_worker(queue, thread_number):
        """

        :param queue:
        :param thread_number:
        :return: None
        """
        while not queue.empty():
            payload = queue.get()
            CourseEvaluationsScraper.crawl_professor_search(payload)
            queue.task_done()

    @staticmethod
    def crawl_professor_search(payload):
        """
        Perform a request to get the course evaluation JSON. Scrape the course evaluation
        details from the HTML returned from the request and add it to the running total
        :param payload: A payload with the page and pageSize
        :return:
        """
        CourseEvaluationsScraper.logger.info("Beginning to scrape %s course evaluations on page %s",
                                        PAGE_SIZE, payload["pageActuelle"])

        all_course_evals_base_url = CourseEvaluationsScraper.HOST + CourseEvaluationsScraper.EVALS_REQUEST_PATH
        all_course_evals_results = Scraper.get_web_page(all_course_evals_base_url, method='POST',
                                                        payload=payload)["d"][0]
        search_soup = BeautifulSoup(all_course_evals_results, features=ENVIRONMENT.bs_parser)
        course_evals = search_soup.find_all("tr", class_="gData")
        for course_eval in course_evals:
            course_id_short = course_eval["sk"]
            fields = course_eval.find_all("td")

            dept_name = fields[DEPT_INDEX].text.strip().split('-')
            prof_name = fields[FNAME_INDEX].text.strip()[0] + " " + fields[LNAME_INDEX].text.strip()
            term = fields[TERM_INDEX].text.strip()
            year = fields[YEAR_INDEX].text.strip()
            instructor_enthusiasm = fields[ENTHUSIASM_INDEX].text.strip()
            workload = fields[WORKLOAD_INDEX].text.strip()
            recommend = fields[RECOMMEND_INDEX].text.strip()
            survey_invitations_count = fields[INVITED_INDEX].text.strip()
            survey_completion_count = fields[COMPLETED_INDEX].text.strip()

            CourseEvaluationsScraper.lock.acquire()
            CourseEvaluationsScraper.RESULTS.append({
                "course_id_short": course_id_short, "dept": dept_name, "term": term, "year": year,
                "professor": prof_name, "instructor_enthusiasm": instructor_enthusiasm, "workload": workload,
                "recommend": recommend, "survey_invitiations": survey_invitations_count,
                "survey_completions": survey_completion_count

            })
            CourseEvaluationsScraper.lock.release()

        CourseEvaluationsScraper.logger.info("Finished scraping %s course evaluations on page %s",
                                        PAGE_SIZE, payload["pageActuelle"])


with open(OUTPUT_PATH, "w") as file:
    file.write(json.dumps(CourseEvaluationsScraper.scrape(), indent=3))