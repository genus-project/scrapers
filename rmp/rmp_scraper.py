import math
import re
import json
import time
from queue import Queue
from threading import Thread, Lock
from bs4 import BeautifulSoup
from profiles import ENVIRONMENT
from helpers.scraper import Scraper


OUTPUT_PATH = "/Users/Ahmed/Desktop/rating_output.json"

PAGINATION_REGEX = "Showing (.*) of (.*) results"
TID_REGEX = "\/ShowRatings\.jsp\?tid=(.*)"
MAX_LIMIT = 20


class RateMyProfessorRatingsScraper:
    """
    A professor ratings scraper for RateMyProfessors.

    This class scrapes the host by performing a blanket search for all professors,
    paginating through all the responses, and scrapes each professor.
    """

    HOST = "http://www.ratemyprofessors.com"

    TID_PATH = "\ShowRatings.jsp"
    TID_PATH_PARAMS = {"tid": ""}

    PROF_SEARCH_PATH = "/search.jsp"
    PROF_SEARCH_PARAMS = {
                    "query": "",
                    "queryoption": "HEADER",
                    "stateselect": "",
                    "country": "",
                    "dept": "",
                    "queryBy": "teacherName",
                    "facetSearch": "",
                    "schoolName": "university+of+toronto+st+george",
                    "offset": "0",
                    "max": "20"
                }

    lock = Lock()
    RESULTS = []
    ERROR_COUNT = 0

    @staticmethod
    def scrape():
        """
        Generate the jobs to scrape the host website, queue them,
        and assign them to workers.
        :return: A list of JSON teacher ratings
        """
        start_time = time.time()
        q = Queue()
        job_count = RateMyProfessorRatingsScraper.get_job_count()

        for i in range(job_count):
            job_params = RateMyProfessorRatingsScraper.PROF_SEARCH_PARAMS.copy()
            job_params["offset"] = str(i * MAX_LIMIT)
            q.put(job_params)
        
        for i in range(ENVIRONMENT.scraper.maxThreads):
            print("Provisioning a new thread")
            t = Thread(target=RateMyProfessorRatingsScraper.scraper_worker, args=(q,i))
            t.start()
        q.join()

        total = len(RateMyProfessorRatingsScraper.RESULTS)
        elapsed_time = time.time() - start_time
        time_taken = time.strftime("%H:%M:%S", time.gmtime(elapsed_time))
        print("Successfully scraped {} with {} errors, total attempts is {}, time taken is {}"
              .format(total, RateMyProfessorRatingsScraper.ERROR_COUNT,
                      total + RateMyProfessorRatingsScraper.ERROR_COUNT, time_taken))

        all_professor_ratings = RateMyProfessorRatingsScraper.RESULTS.copy()
        RateMyProfessorRatingsScraper.RESULTS, RateMyProfessorRatingsScraper.ERROR_COUNT = [], 0
        return all_professor_ratings

    @staticmethod
    def scraper_worker(queue, thread_number):
        """

        :param queue:
        :param thread_number:
        """
        while not queue.empty():
            params = queue.get()
            RateMyProfessorRatingsScraper.crawl_professor_search(params)
            print("Thread {} completed scraping with offset {}".format(str(thread_number), params["offset"]))
            queue.task_done()

    @staticmethod
    def get_job_count():
        """

        :return: The integer number of jobs needed to scrape the host
        """
        all_professors_base_url = RateMyProfessorRatingsScraper.HOST + RateMyProfessorRatingsScraper.PROF_SEARCH_PATH
        professor_search_results = Scraper.get_web_page(all_professors_base_url,
                                                        params=RateMyProfessorRatingsScraper.PROF_SEARCH_PARAMS)
        search_soup = BeautifulSoup(professor_search_results, features=ENVIRONMENT.bs_parser)
        pagination_term_search = re.search(PAGINATION_REGEX, search_soup.find_all("div", class_="result-count")[-1].text,
                                           re.IGNORECASE)
        if not pagination_term_search:
            print("Unable to find result count for a blanket search....")
            raise ValueError
        prof_count = int(pagination_term_search[2])
        job_count = math.ceil(prof_count / MAX_LIMIT)
        return job_count

    @staticmethod
    def get_course_detail_field(name):
        name_list = re.sub(r"\s+", "", name).split(",")
        return "{} {}".format(name_list[1][0], name_list[0])

    @staticmethod
    def crawl_professor_search(params):
        """
        Perform a blank search request to the host and scrape the urls of all the
        professors who appear in this search. Follow the links to the professors' review
        page and append a dictionary representing the data scraped.
        :param params: A dictionary representing the query params for the host
        """
        all_professors_base_url = RateMyProfessorRatingsScraper.HOST + RateMyProfessorRatingsScraper.PROF_SEARCH_PATH
        professor_search_results = Scraper.get_web_page(all_professors_base_url, params=params)
        search_soup = BeautifulSoup(professor_search_results, features=ENVIRONMENT.bs_parser)
        profs = search_soup.find_all("li", class_="listing PROFESSOR")
        print("Beginning to scrape page with offset {}".format(params["offset"]))

        for prof in profs:
            try:
                prof_name = RateMyProfessorRatingsScraper.get_course_detail_field(prof.find("span", class_="main").text)
            except IndexError:
                print("{} is not in the expected format, skipping...".format(prof.find("span", class_="main").text))
                RateMyProfessorRatingsScraper.lock.acquire()
                RateMyProfessorRatingsScraper.ERROR_COUNT += 1
                RateMyProfessorRatingsScraper.lock.release()
                continue
            # Find the prof's name and link to their review page
            prof_href = prof.find("a", href=True)["href"]
            prof_tid = re.search(TID_REGEX, prof_href)[1]
            prof_url = RateMyProfessorRatingsScraper.HOST + prof_href

            professor_reviews = Scraper.get_web_page(prof_url)
            review_soup = BeautifulSoup(professor_reviews, features=ENVIRONMENT.bs_parser)
            rating = review_soup.find("div", class_="rating-breakdown")

            try:
                # Get all the rating information
                overall_quality, repeat, difficulty = [grade.text.strip() for grade in rating.find_all("div", class_="grade")]
                RateMyProfessorRatingsScraper.lock.acquire()
                RateMyProfessorRatingsScraper.RESULTS.append({
                    "name": prof_name, "tid": prof_tid, "quality":overall_quality,
                    "repeat": repeat, "difficulty": difficulty, "url": prof_url
                })
                RateMyProfessorRatingsScraper.lock.release()
            except AttributeError:
                print("{} does not have the properties needed, skipping......".format(prof_url))
                RateMyProfessorRatingsScraper.lock.acquire()
                RateMyProfessorRatingsScraper.ERROR_COUNT += 1
                RateMyProfessorRatingsScraper.lock.release()
        print("Finished scraping page with offset {}".format(params["offset"]))


with open(OUTPUT_PATH, "w") as file:
    file.write(json.dumps(RateMyProfessorRatingsScraper.scrape(), indent=3))