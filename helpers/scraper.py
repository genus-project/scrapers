import requests

from profiles import ENVIRONMENT

CONTENT_TYPE = 'Content-Type'
TEST_PATH_VALID_JSON = "https://www.ratemyprofessors.com/paginate/professors/ratings?tid=18534&page=0&cache=false"
TEST_PATH_VALID = "https://course-evals.utoronto.ca/BPI/fbview.aspx?blockid=-iEZm-QSMJH-py5utu"
TEST_PATH_INVALID = "http://BLAH"


class Scraper:

    session = requests.Session()

    @staticmethod
    def get_web_page(url, headers={}, params={}, method='GET', payload={}):
        attempts, found = 0, False
        while attempts < ENVIRONMENT.scraper.maxAttempts and not found:
            try:
                if method == 'GET':
                    resp = Scraper.session.get(url, headers=headers, params=params)
                elif method == 'POST':
                    resp = Scraper.session.post(url, headers=headers, params=params, json=payload)
                else:
                    raise requests.exceptions.HTTPError
                if resp.status_code == 200:
                    found = True
                else:
                    attempts += 1
            except (requests.exceptions.Timeout,
                    requests.exceptions.ConnectionError,
                    requests.exceptions.InvalidSchema,
                    requests.exceptions.MissingSchema) as e:
                attempts += 1
                print("Exception has occurred when trying to get {}.....", url)
        if found:
            if "application/json" in resp.headers.get(CONTENT_TYPE):
                return resp.json()
            return resp.text.encode('utf-8')
        return None