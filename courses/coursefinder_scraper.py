import re
import sys
import json
import time
import logging
from queue import Queue
from threading import Thread, Lock
from bs4 import BeautifulSoup
from profiles import ENVIRONMENT
from helpers.scraper import Scraper

COURSE_ID_REGEX = "#add(.*)"

OUTPUT_PATH = "/Users/Ahmed/Desktop/course_output.json"

# Lecture Section indices
ACTIVITY_INDEX = 0
TIME_INDEX = 1
INSTRUCTOR_INDEX = 2
LOCATION_INDEX = 3
SIZE_INDEX = 4
DELIVERY_METHOD_INDEX = 7

# Basic logging configuration, will refactor and set in parent
logging.basicConfig(
    level=logging.INFO,
    format="[%(asctime)s][%(levelname)s][%(name)s.%(funcName)s:%(lineno)d] %(message)s",
    datefmt="%H:%M:%S",
    stream=sys.stdout)


class CourseFinderScraper:
    """
    A scraper for the course information at UofT.

    This class scrapes the host by performing a blanket search for all course evaluations,
    paginating through all the responses, and scrapes each evaluation.
    """
    HOST = "http://coursefinder.utoronto.ca"

    COURSE_SEARCH_PATH = "/course-search/search/courseSearch/course/search"
    COURSE_SEARCH_PARAMS = {"queryText": "", "requirements": "", "campusParam": "St. George"}

    COURSE_ID_PATH = "/course-search/search/courseSearch/coursedetails/{}"

    logger = logging.getLogger('course-finder-scraper')

    RESULTS = []
    ERROR_COUNT = 0
    WARN_COUNT = 0
    WARN_SET = set()
    lock = Lock()

    @staticmethod
    def scrape():
        """
        Generate the jobs to scrape the host website, queue them,
        and assign them to workers.
       :return: A list of JSON course information
        """
        start_time = time.time()
        q = Queue()
        all_courses_base_url = CourseFinderScraper.HOST + CourseFinderScraper.COURSE_SEARCH_PATH
        courses_search_results = Scraper.get_web_page(all_courses_base_url,
                                                      params=CourseFinderScraper.COURSE_SEARCH_PARAMS)["aaData"]
        job_count = len(courses_search_results)
        CourseFinderScraper.logger.info("%s jobs found, setting up workers.", job_count)

        for course in courses_search_results:
            course_href = course[0]
            course_search_soup = BeautifulSoup(course_href, features=ENVIRONMENT.bs_parser)
            course_id = re.search(COURSE_ID_REGEX, course_search_soup.find("a")["href"])[1]
            q.put(course_id)

        for i in range(ENVIRONMENT.scraper.maxThreads):
            CourseFinderScraper.logger.info("Provisioning a new thread")
            t = Thread(target=CourseFinderScraper.scraper_worker, args=(q,i))
            t.start()
        q.join()

        total = len(CourseFinderScraper.RESULTS)
        elapsed_time = time.time() - start_time
        time_taken = time.strftime("%H:%M:%S", time.gmtime(elapsed_time))
        CourseFinderScraper.logger.info("Successfully scraped %s with %s errors, %s warnings, total attempts is %s, "
                                        "time taken is %s", total, CourseFinderScraper.ERROR_COUNT,
                                        CourseFinderScraper.WARN_COUNT, total + CourseFinderScraper.ERROR_COUNT,
                                        time_taken)

        all_course_information = CourseFinderScraper.RESULTS.copy()
        CourseFinderScraper.RESULTS, CourseFinderScraper.ERROR_COUNT, CourseFinderScraper.WARN_COUNT = [], 0, 0
        return all_course_information

    @staticmethod
    def scraper_worker(queue, thread_number):
        """

       :param queue:
       :param thread_number:
       :return: None
        """
        while not queue.empty():
            url = queue.get()
            try:
                CourseFinderScraper.crawl_course_search(url)
            except Exception:
                CourseFinderScraper.logger.warning("Exception surfaced up with url %s", format(url))
            finally:
                CourseFinderScraper.logger.info("Thread %s completed a job", thread_number)
                queue.task_done()

    @staticmethod
    def crawl_course_search(course_id):
        """
        Perform a request to get the course information webpage. Scrape the course id
        details from the webpage and add it to the running total
        :param course_id: A unique course_id that maps to a webpage with this course's information
        :return:
        """
        CourseFinderScraper.logger.info("Beginning to scrape course with id - %s", course_id)

        course_details_url = CourseFinderScraper.HOST + CourseFinderScraper.COURSE_ID_PATH.format(course_id)
        course_details_results = Scraper.get_web_page(course_details_url)
        course_soup = BeautifulSoup(course_details_results, features=ENVIRONMENT.bs_parser)
        course_header = [field.strip() for field in course_soup.find("span", class_="uif-headerText-span").text.split(":")]

        course_id_short, course_title = course_header[0], "".join(course_header[1:])
        division = CourseFinderScraper.get_course_detail_field(course_soup, course_id, "span", {"id": "u23"})
        description = CourseFinderScraper.get_course_detail_field(course_soup, course_id, "span", {"id": "u32"})
        department = CourseFinderScraper.get_course_detail_field(course_soup, course_id, "span", {"id": "u41"})
        exclusion = CourseFinderScraper.get_course_detail_field(course_soup, course_id, "span", {"id": "u68"})
        level = CourseFinderScraper.get_course_detail_field(course_soup, course_id, "span", {"id": "u86"})
        breadth = CourseFinderScraper.get_course_detail_field(course_soup, course_id, "span", {"id": "u122"})
        campus = CourseFinderScraper.get_course_detail_field(course_soup, course_id, "span", {"id": "u149"})
        term = CourseFinderScraper.get_course_detail_field(course_soup, course_id, "span", {"id": "u158"})

        sections = {}
        lectures, tutorials, practicals = [], [], []
        for index, section_information in enumerate(course_soup.find_all("tr")[1:]):
            # First section is column definitions
            current_section = section_information.find_all("td")
            activity = current_section[ACTIVITY_INDEX].text.strip()
            if not activity.startswith('Lec'):
                continue

            space_sep_date_times = current_section[TIME_INDEX].text.strip().split()
            space_sep_locations = current_section[LOCATION_INDEX].text.strip().split()
            space_sep_professors = current_section[INSTRUCTOR_INDEX].text.strip().split()
            # Courses that have multiple lectures per section are space seperated, transform this
            # [M 10-12 T 3-4] -> [M 10-12, T 3-4]
            date_times, locations, professors = [], [], []
            span = 2
            for i in range(0, len(space_sep_date_times), span):
                date_times.append(" ".join(space_sep_date_times[i:i+span]))

            for i in range(0, len(space_sep_locations), span):
                locations.append(" ".join(space_sep_locations[i:i+span]))

            for i in range(0, len(space_sep_professors), span):
                professors.append(" ".join(space_sep_professors[i:i+span]))

            class_size = current_section[SIZE_INDEX].text.strip()
            delivery = current_section[DELIVERY_METHOD_INDEX].text.strip()
            lectures.append({
                "activity": activity, "professor": professors, "time": date_times, "size": class_size,
                "locations": locations, "delivery": delivery
            })

        sections["lecture"] = lectures
        CourseFinderScraper.lock.acquire()
        CourseFinderScraper.RESULTS.append({
            "course_id_short": course_id_short, "course_id_long": course_id, "title": course_title,
            "division": division, "description": description, "department": department, "exclusion": exclusion,
            "level": level, "breadth": breadth, "campus": campus, "term": term, "sections": sections
        })
        CourseFinderScraper.lock.release()
        CourseFinderScraper.logger.info("Finished scraping course with id   - %s", course_id)

    @staticmethod
    def get_course_detail_field(soup, course_id, tag, arg_dict):
        try:
            result = soup.find(tag, attrs=arg_dict).text.strip()
            return result
        except AttributeError:
            CourseFinderScraper.WARN_COUNT += 1
            return None


with open(OUTPUT_PATH, "w") as file:
    file.write(json.dumps(CourseFinderScraper.scrape(), indent=3))